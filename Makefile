CPPFLAGS+=-g -pedantic -Wall -Werror -std=c++17 
HW=$(shell basename ${PWD})

main: main.cpp
	${CXX} ${CPPFLAGS} -o $@ $^

test:
	bash code_test.sh

zip: main.cpp
	zip ${HW}.zip $^

clean:
	rm -f *.o
	rm -f main
	rm -f ${HW}.zip

.PHONY: clean zip test