#!/bin/bash

#-----------print functions----------
BOLD='\033[1m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color
function error(){
	echo -e "${RED}ERROR: $1${NC}"
}

function status(){
	echo -n -e "${BOLD}$1${NC}.. "
}

function ok(){
	echo -e "${GREEN}OK${NC}"
}
#-----------print functions----------


#arguments check
if [ -z "$1" ]; then
	echo -e "${RED}Please provide HW name!${NC} for example HW01"
	exit
fi



hw="$1"   #hw (homework) name
hw_lower="$(echo $hw | tr '[:upper:]' '[:lower:]')"   #hw in lowercase, for prpa git
tools_dir="ppc-tools"   #location of prpa-tools - this script, makefile, etc.
ppc_git="ppc"    #prpa git repository


#check wroking directory of this script and eventually go one level back
if [ "$(basename $(pwd))" == "$tools_dir" ]; then
	echo "Changing dir, 'cd ..'"
	cd ..
fi


#create HW directory
status "Creating directory '$hw'"
mkdir -p "$hw" && ok

#copy main.c template from prpa-tools
status "Creating main.cpp template"
if [ -f "$hw/main.cpp" ]; then   #we don't want to overwrite existing main.cpp
	error "main.cpp file already exists"
else
	cp "$tools_dir/main.cpp.template" "$hw/main.cpp" && ok
fi

#copy makefile
status "Copying makefile and test"
if [ -f "$hw/Makefile" ]; then   #we don't want to overwrite existing Makefile
	error "Makefile already exists"
else
	cp "$tools_dir/Makefile" "$hw" && ok
fi

#copy program test script
status "Copying test script" && \
cp "$tools_dir/code_test.sh" "$hw" && ok

#create directory for testing data
status "Creating testing data directory"
mkdir -p "$hw/data" && ok

#update prpa git repository
status "Pulling prpa git"
cd "$ppc_git" && git pull && cd .. && ok

#copy testing data from prpa git to the new HW dir
status "Trying to obtain testing data"
cp -r "ppc/homeworks/$hw_lower/data/" "$hw/" && ok
