# ppc-tools

Tools for Practical C/C++ programming 


## Directory structure

```
.
├── HWxx
├── ppc
└── ppc-tools
    └── test_data
        └── HWxx
            └── data
```
- `HWxx` are directories with source codes
- `ppc` is submodule directory with ppc codes
- `ppc-tools` is this repository (submodule)
- `ppc-tools/test_data` are test data for HWxx


## Tools

- `create_hw.sh` - script for creating homework directory, copying data from prpa git dir and copying tools from this repo 
- `code_test.sh` - script for `diff` code testing homework codes using inputs and expected outputs from prpa-codes
- `Makefile` - Makefile for make
- `main.cpp.template` - default `main.cpp` template


## Usage

- call `bash create_hw.sh HW01` to create new HW
- change directory to the new HW `cd HW01`
- compile program `make`
- test `make test`
- create zip archive `make zip` and submit it
- cleanup `make clean`


## Makefile

- `main` - compiles code
- `test` - runs `code_test.sh` script
- `zip` - create zip file for BRUTE system
- `clean` - cleanup